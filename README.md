# Hosting Test - GitLab Pages

This is a test of hosting a Hugo site on GitLab Pages.

The site requires:

- The dart-sass-embedded executable to transpile Sass to CSS
- Installation of Node.JS dependencies
